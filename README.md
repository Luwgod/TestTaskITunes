# iTunes albums fetch Test Task

## About:
Simple project to look up for albums with iTunes api

## Features:
1. Use text field to search for album by name
2. You can see your search history and interact with it
3. Open album information with songs by clicking on it

## Used technologies:
1. UIKit
2. MVC
3. iTunes api (URLSession)
4. Core Data

