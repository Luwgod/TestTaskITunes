//
//  AlbumInfoViewController.swift
//  ForaSoftITunes
//
//  Created by Sasha Styazhkin on 15.11.2021.
//

import UIKit

class AlbumInfoViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    
    @IBOutlet weak var albumImageView: UIImageView!
    
    @IBOutlet weak var tracksCollectionView: UICollectionView!
    
    var tracks = [Track]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tracksCollectionView.dataSource = self
        tracksCollectionView.delegate = self
        
        albumImageView.layer.cornerRadius = 20
        
    }
    
    func fetchTracks(collectionId: Int, fetchCompletion: @escaping () -> ()) {
        let group = DispatchGroup()
        
        let urlString = "https://itunes.apple.com/lookup?id=\(collectionId)&entity=song"
        group.enter()
        NetworkDataFetch.shared.fetchTrack(urlString: urlString) { [weak self] trackModel, error in
            if error != nil {
                print(error?.localizedDescription)
            } else {
                guard let trackModel = trackModel else { return }
                
                self?.tracks = Array(trackModel.results.dropFirst())
                self?.tracksCollectionView.reloadData()
        
                group.leave()
            }
            
        }
        
        group.notify(queue: .main){
            fetchCompletion()
        }
        
    }
    
    
    

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = tracksCollectionView.dequeueReusableCell(withReuseIdentifier: "trackCell", for: indexPath) as! TrackCollectionViewCell
        cell.configureSongCell(track: tracks[indexPath.row])
        cell.trackNumberLabel.text = String(indexPath.row + 1)
        return cell
    }
    
    
    func configureAlbumInfoVc(collectionId: Int, configureCompletion: @escaping () -> Void) {
        fetchTracks(collectionId: collectionId){
            configureCompletion()
        }
    }


}


// https://itunes.apple.com/lookup?id=1418213110&entity=song
