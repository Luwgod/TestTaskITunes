//
//  SongCollectionViewCell.swift
//  ForaSoftITunes
//
//  Created by Sasha Styazhkin on 15.11.2021.
//

import UIKit

class TrackCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var trackNumberLabel: UILabel!
    
    
    func configureSongCell(track: Track) {
        trackNameLabel.text = track.trackName
        artistNameLabel.text = track.artistName
    }
    
    
}
