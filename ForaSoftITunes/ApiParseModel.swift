//
//  ApiParseModel.swift
//  ForaSoftITunes
//
//  Created by Sasha Styazhkin on 26.10.2021.
//

import Foundation

struct AlbumModel: Decodable {
    let results: [Album]
}

struct Album: Decodable {
    let artistName: String
    let collectionId: Int
    let collectionName: String
    let artworkUrl100: String?
    let trackCount: Int
    let releaseDate: String
}

struct TrackModel: Decodable {
    let resultCount: Int
    let results: [Track]
}

struct Track: Decodable {
    let trackName: String?
    let artistName: String
}
