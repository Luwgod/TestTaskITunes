//
//  HistoryTableViewCell.swift
//  ForaSoftITunes
//
//  Created by Sasha Styazhkin on 19.11.2021.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    
    @IBOutlet weak var historyTextLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
