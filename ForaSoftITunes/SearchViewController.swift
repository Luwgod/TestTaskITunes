//
//  SearchViewController.swift
//  ForaSoftITunes
//
//  Created by Sasha Styazhkin on 26.10.2021.
//

import UIKit

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet weak var albumsCollectionView: UICollectionView!
    
    
    @IBOutlet weak var historyTableView: UITableView!
    
    
    @IBOutlet weak var albumSearchBar: UISearchBar!
    
    
    var albums: [Album]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSavedItems()
        
        albumsCollectionView.delegate = self
        albumsCollectionView.dataSource = self
        historyTableView.delegate = self
        historyTableView.dataSource = self
        albumSearchBar.delegate = self
        
        fetchAlbums(albumName: "Scorpion")
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        
    }
    

    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = albumsCollectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCell", for: indexPath) as! AlbumCollectionViewCell
        if let albums = albums {
            cell.configureAlbumCell(album: albums[indexPath.row])
        }
        return cell
    }
    
    
    func fetchAlbums(albumName: String) {
        let urlString = "https://itunes.apple.com/search?term=\(albumName)&entity=album&attribute=albumTerm"
        
        NetworkDataFetch.shared.fetchAlbum(urlString: urlString) { [weak self] albumModel, error in
            
            if error != nil {
                print(error?.localizedDescription)
            } else {
                guard let albumModel = albumModel else { return }
                
                self?.albums = albumModel.results
                self?.albumsCollectionView.reloadData()
            }
        }
    }
    
    

    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print(albums?[indexPath.row].artistName)
        
        let cell = collectionView.cellForItem(at: indexPath) as? AlbumCollectionViewCell
        
        UIView.animate(withDuration: 0.2, animations: { cell?.alpha = 0.5 }) { (completed) in
                UIView.animate(withDuration: 0.2, animations: { cell?.alpha = 1 } )
            }
        
        let albumInfoVC = storyboard?.instantiateViewController(identifier: "AlbumInfoViewController") as! AlbumInfoViewController
        _ = albumInfoVC.view
        guard let collectiondId = albums?[indexPath.row].collectionId else { return }
        albumInfoVC.configureAlbumInfoVc(collectionId: collectiondId) {
            albumInfoVC.albumImageView.image = cell?.albumImageView.image
            self.present(albumInfoVC, animated: true, completion: nil)
        }
    }
    
    
    @objc func loadList(notification: NSNotification){
        getSavedItems()
        DispatchQueue.main.async {
            self.historyTableView.reloadData()
        }
    }
    

}


extension SearchViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        historyTableView.isHidden = !(historyTableView.isHidden)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchedText = searchBar.text {
            if searchedText.isEmpty { return }
            saveItem(text: searchedText)
            fetchAlbums(albumName: searchedText)
            print("Searching album")
            historyTableView.isHidden = true
            searchBar.endEditing(true)
            searchBar.setShowsCancelButton(false, animated: true)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchBar.setShowsCancelButton(false, animated: true)
        historyTableView.isHidden = !(historyTableView.isHidden)
    }
    
    
}


extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyTableViewCell") as! HistoryTableViewCell
        cell.historyTextLabel.text = models[indexPath.row].text
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteItem(item: models[indexPath.row])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let searchedText = models[indexPath.row].text else {
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        albumSearchBar.text = searchedText
        albumSearchBar.setShowsCancelButton(false, animated: true)
        fetchAlbums(albumName: searchedText)
        historyTableView.isHidden = true
        albumSearchBar.endEditing(true)
        
    }
    
}
