//
//  NetworkDataFetch.swift
//  ForaSoftITunes
//
//  Created by Sasha Styazhkin on 26.10.2021.
//

import Foundation

class NetworkDataFetch {
    
    static let shared = NetworkDataFetch()
    
    private init() {}
    
    func fetchAlbum (urlString: String, responce: @escaping (AlbumModel?, Error?) -> Void) {
        
        NetworkRequest.shared.requestData(urlString: urlString) { result in
            switch result {
            
            case .success(let data):
                do {
                    let albums = try JSONDecoder().decode(AlbumModel.self, from: data)
                    responce(albums, nil)
                } catch let jsonError {
                    print("JSON decode failure.", jsonError)
                }
                
            case .failure(let error):
                print("Error with data.", error.localizedDescription)
                responce(nil, error)
                
            }
        }
        
    }
    
    
    func fetchTrack (urlString: String, responce: @escaping (TrackModel?, Error?) -> Void) {
        
        NetworkRequest.shared.requestData(urlString: urlString) { result in
            switch result {
            
            case .success(let data):
                do {
                    let tracks = try JSONDecoder().decode(TrackModel.self, from: data)
                    responce(tracks, nil)
                } catch let jsonError {
                    print("JSON decode failure.", jsonError)
                }
                
            case .failure(let error):
                print("Error with data.", error.localizedDescription)
                responce(nil, error)
                
            }
        }
        
    }
    
}
