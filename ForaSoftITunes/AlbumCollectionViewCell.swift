//
//  AlbumCollectionViewCell.swift
//  ForaSoftITunes
//
//  Created by Sasha Styazhkin on 26.10.2021.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    
    
    func configureAlbumCell(album: Album) {
        if let urlString = album.artworkUrl100 {
            NetworkRequest.shared.requestData(urlString: urlString) { [weak self] result in
                switch result {
                case .success(let data):
                    let image = UIImage(data: data)
                    self?.albumImageView.image = image
                case .failure(let error):
                    self?.albumImageView.image = nil
                    print("No album image.", error.localizedDescription)
                }
            }
        } else {
            albumImageView.image = nil
        }
        albumNameLabel.text = album.collectionName
        artistNameLabel.text = album.artistName
    }
    
    
    
    
}
