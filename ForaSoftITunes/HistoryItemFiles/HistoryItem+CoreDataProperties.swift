//
//  HistoryItem+CoreDataProperties.swift
//  ForaSoftITunes
//
//  Created by Sasha Styazhkin on 19.11.2021.
//
//

import Foundation
import CoreData


extension HistoryItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<HistoryItem> {
        return NSFetchRequest<HistoryItem>(entityName: "HistoryItem")
    }

    @NSManaged public var text: String?

}

extension HistoryItem : Identifiable {

}
