//
//  Model.swift
//  ForaSoftITunes
//
//  Created by Sasha Styazhkin on 19.11.2021.
//

import Foundation
import SwiftUI
import CoreData

var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
var models = [HistoryItem]()


func getSavedItems() {
    do {
        models = try context.fetch(HistoryItem.fetchRequest())
    } catch {
    }
}

func saveItem(text: String) {
    let newItem = HistoryItem(context: context)
    newItem.text = text
    
    models.append(newItem)
    
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
    do {
        try context.save()
    } catch {
         print("Some error in saving data")
    }
}

func deleteItem(item: HistoryItem) {
    context.delete(item)
    do {
        try context.save()
    } catch {
         print("Some error in deleting data")
    }
}

